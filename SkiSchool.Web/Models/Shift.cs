﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SkiSchool.Web.Models
{
    public class Shift
    {
        public string Name { get; set; }

        public DateTime Date { get; set; }

        public DateTime Start { get; set; }

        public string StartTime { get; set; }

        public DateTime End { get; set; }

        public string EndTime { get; set; }

        public int Id { get; set; }

        public List<ShiftGroup> ShiftGroups { get; set; }
    }
}