﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SkiSchool.Web.Models
{
    public class EmployeeSchedule
    {
        public DateTime Date { get; set; }

        public DateTime Start { get; set; }

        public string StartTime { get; set; }

        public DateTime End { get; set; }

        public string EndTime { get; set; }

        public int Id { get; set; }

        public string RosterId { get; set; }

        public int? EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Priority Priority { get; set; }

        public Season Season { get; set; }

        public ShiftType ShiftType { get; set; }

        public int ScheduleTimeId { get; set; }

        public int Count { get; set; }
    }
}