﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SkiSchool.Web.Models
{
    public class ShiftGroup
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int Id { get; set; }

        public List<EmployeeSchedule> Schedules { get; set; }
    }
}