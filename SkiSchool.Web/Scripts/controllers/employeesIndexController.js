﻿var module = angular.module("employeesIndex", []);

module.config(function ($routeProvider) {
    $routeProvider.when('/', {
        controller: 'employeesController',
        templateUrl: '../../Templates/employeesView.html'
    });

    $routeProvider.otherwise({ redirectTo: '/' });
});

var employeesController = ['$scope', 'employeesService', 'scheduleService', function($scope, employeesService, scheduleService) {
    $scope.data = employeesService;
    $scope.isLoading = false;
    $scope.sortorder = 'Person.LastName';
    $scope.sortorderdirection = '+';
    $scope.isDeletingShift = false;

    if (employeesService.isReady() == false) {
        $scope.isLoading = true;
        employeesService.getEmployees()
              .then(function () {
                  // success
              },
              function () {
                  // error
                  alert('could not load.');
              })
              .then(function () {
                  $scope.isLoading = false;
              });

        employeesService.getEmployeeTitles()
              .then(function () {
                  // success
              },
              function () {
                  // error
                  alert('could not load.');
              })
              .then(function () {
                  $scope.isLoading = false;
              });

        employeesService.getEmployeeTypes()
              .then(function () {
                  // success
              },
              function () {
                  // error
                  alert('could not load.');
              })
              .then(function () {
                  $scope.isLoading = false;
              });

        employeesService.getGenders()
                .then(function () {
                    // success
                },
                function () {
                    // error
                    alert('could not load');
                })
                .then(function () {
                    $scope.isLoading = false;
                });
    }

    $scope.showShifts = function (item) {
        $scope.selectedItem = item;
        $scope.isModalLoading = true;

        employeesService.getEmployeeShifts(item)
                .then(function () {
                    // success
                },
                function () {
                    // error
                    alert('could not load');
                })
                .then(function () {
                    $scope.isModalLoading = false;
                });

        $('#viewEmployeeSchedule').modal({});
    }

    $scope.showDetails = function (item) {
        $scope.selectedItem = item;
        $('#viewEmployee').modal({});
    }

    $scope.showAddEmployee = function () {
        $scope.newEmployee = null;
        $('#addEmployee').modal({});
    }

    $scope.editDetails = function (item) {
        $scope.selectedItem = item;
        $('#editEmployee').modal({});
    }

    $scope.deleteDetails = function (item) {
        $scope.selectedItem = item;
        $('#deleteEmployee').modal({});
    }

    $scope.editEmployee = function (item) {
        $scope.isLoading = false;

        employeesService.editEmployee(item)
              .then(function () {
                  // success
              },
              function () {
                  // error
                  alert('could not save.');
              })
              .then(function () {
                  $scope.isLoading = false;

                  employeesService.getEmployees();
              });
    }

    $scope.addEmployee = function (item) {
        $scope.isLoading = false;

        employeesService.addEmployee(item)
              .then(function () {
                  // success
              },
              function () {
                  // error
                  alert('could not save.');
              })
              .then(function () {
                  $scope.isLoading = false;

                  employeesService.getEmployees();
              });
    }

    $scope.removeShift = function (item) {
        
        $scope.isDeletingShift = true;
        item.EmployeeId = null;

        scheduleService.putSchedule(item)
                        .then(function () {
                            // success
                        },
                        function () {
                            // error
                            alert('could not delete.');
                        })
                        .then(function () {
                            $scope.isLoading = false

                            var selectedEmployee = $scope.selectedItem;

                            employeesService.getEmployeeShifts(selectedEmployee)
                                .then(function () {
                                    // success
                                },
                                function () {
                                    // error
                                    alert('could not load');
                                })
                                .then(function () {
                                    $scope.isDeletingShift = false;
                                });
                        });
    }

}];