﻿var module = angular.module('schedulesPrint', []);

module.config(function ($routeProvider) {
    $routeProvider.when('/', {
        controller: 'schedulesPrintController',
        templateUrl: '../../Templates/schedulesPrintView.html'
    });

    $routeProvider.otherwise({ redirectTo: '/' });
});

var schedulesPrintController = ['$scope', 'scheduleService', function ($scope, scheduleService) {
    $scope.data = scheduleService;
    $scope.isLoading = true;
    $scope.isModalLoading = true;


    $scope.scheduleDate = function () {
        var url = document.URL;
        var start = url.lastIndexOf("=") + 1;
        var end = start + 10;
        var date = url.substring(start, end);

        return date;
    }();

    $('.navbar').remove();

    $('footer').remove();


    if (scheduleService.isReady() == false) {
        $scope.isLoading = true;

        var selectedDate = function () {
            var url = document.URL;
            var start = url.lastIndexOf("=") + 1;
            var end = start + 10;
            var date = url.substring(start, end);

            return date;
        }();

        var selectedYear = selectedDate.substring(0, 4);
        var selectedMonth = selectedDate.substring(5, 7);
        var selectedDay = selectedDate.substring(8, 10);

        $scope.formattedScheduleDate = moment(selectedDate).format('dddd, MMMM Do YYYY');

        scheduleService.getDailyPrintSchedules(selectedYear, selectedMonth, selectedDay)
                       .then(function () {
                           // success
                       },
                        function () {
                            // error
                            alert('could not load.');
                        })
                        .then(function () {

                            $scope.isModalLoading = false;
                        });

    }

}];