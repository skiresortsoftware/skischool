﻿var module = angular.module('schedulesCalendar', []);

module.config(function ($routeProvider) {
    $routeProvider.when('/', {
        controller: 'schedulesCalendarController',
        templateUrl: '../../Templates/schedulesCalendarView.html'
    });

    $routeProvider.otherwise({ redirectTo: '/' });
});

var schedulesCalendarController = ['$scope', 'scheduleService', function ($scope, scheduleService) {
    $scope.data = scheduleService;
    $scope.isLoading = true;
    $scope.isModalLoading = true;

    if (scheduleService.isReady() == false) {
        $scope.isLoading = true;
        scheduleService.getSelectedCalendarSchedules()
              .then(function () {
                  // success
              },
              function () {
                  // error
                  alert('could not load.');
              })
              .then(function () {
                  $scope.isLoading = false;

                  $('#calendar').fullCalendar({
                      events: scheduleService.selectedCalendarSchedules,
                      dayClick: function (date, allDay, jsEvent, view) {

                          $scope.selectedDate = $.fullCalendar.formatDate(date, "yyyy-MM-dd");

                          var selectedYear = $.fullCalendar.formatDate(date, "yyyy");
                          var selectedMonth = $.fullCalendar.formatDate(date, "MM");
                          var selectedDay = $.fullCalendar.formatDate(date, "dd");

                          scheduleService.getDailySchedules(selectedYear, selectedMonth, selectedDay)
                                         .then(function () {
                                            // success
                                          },
                                          function () {
                                            // error
                                            alert('could not load.');
                                          })
                                          .then(function () {
                                              $scope.isModalLoading = false;
                                          });

                          $('#viewDailySchedule').modal({});
                      },
                      eventClick: function (calEvent, jsEvent, view) {
                      

                      }
                  })

              });
    }

    $scope.closeDailySchedule = function () {
        $scope.isModalLoading = true;
        $scope.data.dailySchedules.length = 0;
        $scope.selectedDate = null;
    };

    $scope.showDetails = function (item) {
        $scope.selectedItem = item;
        $('#viewDailySchedule').modal({});
    }
}];