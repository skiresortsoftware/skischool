﻿module.factory('scheduleService', function ($http, $q) {

    'use strict';

    var _schedules = [];
    var _dailySchedules = [];
    var _dailyPrintSchedules = [];
    var _aggregateSchedules = [];
    var _selectedSchedules = [];
    var _selectedCalendarSchedules = [];
    var _scheduleTypes = [];
    var _scheduleTimes = [];
    var _isInit = false;
    var _isReady = function () {
        return _isInit;
    };

    var _getSchedulesAggregate = function () {

        var deffered = $q.defer();

        $http.get('../../api/schedules?grouped=true')
               .then(function (result) {
                   // success
                   angular.copy(result.data, _aggregateSchedules);
                   _isInit = true;
                   deffered.resolve();
               }, function () {
                   // error
                   deffered.reject();
               });

        return deffered.promise;
    };

    var _getScheduleTypes = function () {

        var deffered = $q.defer();

        $http.get('../../api/scheduleTypes')
               .then(function (result) {
                   // success
                   angular.copy(result.data, _scheduleTypes);
                   _isInit = true;
                   deffered.resolve();
               }, function () {
                   // error
                   deffered.reject();
               });

        return deffered.promise;
    };

    var _getScheduleTimes = function () {

        var deffered = $q.defer();

        $http.get('../../api/scheduletimes')
             .then(function (result) {
                 // success
                 angular.copy(result.data, _scheduleTimes);
                 deffered.resolve();
             }, function () {
                 // error
                 deffered.reject();
             });

        return deffered.promise;
    };

    var _getDailySchedules = function (year, month, day) {
        
        var deferred = $q.defer();

        $http.get('../../api/schedules?year=' + year + '&month=' + month + '&day=' + day)
             .then(function (result) {
                 // success
                 angular.copy(result.data, _dailySchedules);
                 deferred.resolve();
             }, function () {
                 // error
                 deferred.reject();
             });

        return deferred.promise;
    };

    var _getDailyPrintSchedules = function (year, month, day) {

        var deferred = $q.defer();

        $http.get('../../api/schedules?year=' + year + '&month=' + month + '&day=' + day + '&printable=true')
             .then(function (result) {
                 // success
                 angular.copy(result.data, _dailyPrintSchedules);
                 deferred.resolve();
             }, function () {
                 // error
                 deferred.reject();
             });

        return deferred.promise;
    };


    var _getSchedules = function () {

        var deffered = $q.defer();

        $http.get('../../api/schedules?grouped=false')
               .then(function (result) {
                   // success
                   angular.copy(result.data, _schedules);
                   _isInit = true;
                   deffered.resolve();
               }, function () {
                   // error
                   deffered.reject();
               });

        return deffered.promise;
    };

    var _getSelectedSchedules = function () {

        var deffered = $q.defer();

        $http.get('../../api/schedules')
               .then(function (result) {
                   // success
                   angular.copy(result.data, _selectedSchedules);
                   _isInit = true;
                   deffered.resolve();
               }, function () {
                   // error
                   deffered.reject();
               });

        return deffered.promise;
    };

    var _getSelectedCalendarSchedules = function () {

        var deffered = $q.defer();

        $http.get('../../api/schedules?calendar=true')
               .then(function (result) {
                   // success
                   angular.copy(result.data, _selectedCalendarSchedules);
                   _isInit = true;
                   deffered.resolve();
               }, function () {
                   // error
                   deffered.reject();
               });

        return deffered.promise;
    };

    var _postSchedule = function (schedule) {
        var deffered = $q.defer();

        $http.post('../../api/schedules', schedule)
             .then(function (result) {
                 // success
                 _isInit = true;
                 deffered.resolve();
             }, function () {
                 // error
                 deffered.reject();
             });

        return deffered.promise;
    }

    var _deleteSchedule = function (schedule) {
        var deferred = $q.defer();

        $http.delete('../../api/schedules?id=' + schedule.Id)
             .then(function (result) {
                 // success
                 deferred.resolve();
             }, function () {
                 // error
                 deferred.reject();
             });

        return deferred.promise;
    }
    
    var _putSchedule = function (schedule) {
        var deferred = $q.defer();

        $http.put('../../api/schedules?scheduleId=' + schedule.Id, schedule)
             .then(function (result) {
                 // success
                 deferred.resolve();
             }, function () {
                 // error
                 deferred.reject();
             });

        return deferred.promise;
    }

    return {
        schedules: _schedules,
        aggregateSchedules: _aggregateSchedules,
        dailySchedules: _dailySchedules,
        dailyPrintSchedules: _dailyPrintSchedules,
        selectedSchedules: _selectedSchedules,
        selectedCalendarSchedules: _selectedCalendarSchedules,
        scheduleTypes: _scheduleTypes,
        scheduleTimes: _scheduleTimes,
        getSchedulesAggregate: _getSchedulesAggregate,
        getSchedules: _getSchedules,
        getDailySchedules: _getDailySchedules,
        getDailyPrintSchedules: _getDailyPrintSchedules,
        getScheduleTypes: _getScheduleTypes,
        getScheduleTimes: _getScheduleTimes,
        getSelectedSchedules: _getSelectedSchedules,
        getSelectedCalendarSchedules: _getSelectedCalendarSchedules,
        deleteSchedule: _deleteSchedule,
        putSchedule: _putSchedule,
        postSchedule: _postSchedule,
        isReady: _isReady
    };
});