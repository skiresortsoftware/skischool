﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Http;
using System.Web.Mvc;
using SkiSchool.Web.Filters;

namespace SkiSchool.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SchedulesController : Controller
    {
        //
        // GET: /Schedules/

        public ActionResult Index()
        {
            ViewBag.EmployeeId = HttpContext.Session["employeeId"].ToString();

            return View();
        }

        //
        // GET: /Schedules/Details

        public ActionResult Details()
        {
            ViewBag.EmployeeId = HttpContext.Session["employeeId"].ToString();

            return View();
        }

        // GET: /Schedules/Selected

        public ActionResult Selected()
        {
            ViewBag.EmployeeId = HttpContext.Session["employeeId"].ToString();

            return View();
        }

        // GET: /Schedules/Calendar

        public ActionResult Calendar()
        {
            ViewBag.EmployeeId = HttpContext.Session["employeeId"].ToString();

            return View();
        }

        // GET: /Schedules/Print

        public ActionResult Print(DateTime date)
        {
            ViewBag.EmployeeId = HttpContext.Session["employeeId"].ToString();

            return View();
        }

    }
}
