﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SkiSchool.Web.App_Start;
using SkiSchool.Web.Helpers;
using SkiSchool.Web.Models;

namespace SkiSchool.Web.Controllers.Api
{
    public class PeopleController : ApiController
    {
        private readonly string _clientToken = Config.ClientToken;

        private string _peopleUrl = ApiRoutes.People;

        private string _personUpdateUrl = ApiRoutes.UpdatePerson;

        private string _personAddUrl = ApiRoutes.AddPerson;


        // PUT api/people/{id}
        [HttpPut]
        public Person Put([FromUri]int id, [FromBody]Person person)
        {
            HttpStatusCode httpStatusCode;

            var personUpdateUrl = string.Format(_personUpdateUrl, id, _clientToken);

            var personUpdateUri = new Uri(personUpdateUrl);

            var updatedPerson = Invoke.Put<Person>(personUpdateUri, person, out httpStatusCode);

            return updatedPerson;
        }

        // POST api/people
        [HttpPost]
        public Person Post([FromBody]Person person)
        {
            HttpStatusCode httpStatusCode;

            var personAddUrl = string.Format(_personAddUrl, _clientToken);

            var personAddUri = new Uri(personAddUrl);

            var addedPerson = Invoke.Post<Person>(personAddUri, person, out httpStatusCode);

            return addedPerson;
        }

    }
}
