﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using SkiSchool.Web.App_Start;
using SkiSchool.Web.Helpers;
using SkiSchool.Web.Models;

namespace SkiSchool.Web.Controllers.Api
{
    public class SchedulesController : ApiController
    {
        private readonly string _clientToken = Config.ClientToken;

        private string _employeeSchedules = ApiRoutes.EmployeeSchedules;

        private string _employeesUrl = ApiRoutes.Employees;

        private string _availableSchedules = ApiRoutes.AvailableSchedules;

        private string _updateScheduleWithEmployeeId = ApiRoutes.UpdateScheduleWithEmployeeIdRoute;

        private string _postSchedule = ApiRoutes.PostSchedule;

        private string _allSchedules = ApiRoutes.AllSchedules;

        private string _scheduleTimeUrl = ApiRoutes.ScheduleTime;

        private string _deleteScheduleUrl = ApiRoutes.DeleteSchedule;

        private string _schedulesByDateUrl = ApiRoutes.SchedulesByDate;

        // GET api/schedules?selected=true
        public List<EmployeeSchedule> GetSelectedSchedules()
        {

            HttpStatusCode httpStatusCode;

            var allSchedulesUri = new Uri(string.Format(_allSchedules, _clientToken));

            var allSchedules = Invoke.Get<List<Schedule>>(allSchedulesUri, out httpStatusCode);

            var schedules = allSchedules.Where(s => s.EmployeeId != null)
                                        .OrderBy(s => s.ShiftType.Id);

            var employeesUri = new Uri(string.Format(_employeesUrl, _clientToken));

            var employees = Invoke.Get<List<Employee>>(employeesUri, out httpStatusCode);

            var employeeSchedules = schedules.Join(employees, s => s.EmployeeId, e => e.Id, (schedule, employee) => new { schedule, employee })
                                             .Where(se => se.schedule.EmployeeId == se.employee.Id) 
                                             .Select(s => new EmployeeSchedule
                                             {
                                                 Date = s.schedule.Date,
                                                 Start = s.schedule.Start,
                                                 StartTime = s.schedule.Start.ToString("h:mm tt"),
                                                 End = s.schedule.End,
                                                 EndTime = s.schedule.End.ToString("h:mm tt"),
                                                 ShiftType = s.schedule.ShiftType,
                                                 EmployeeId = s.employee.Id,
                                                 FirstName = s.employee.Person.FirstName,
                                                 LastName = s.employee.Person.LastName
                                             });

            return employeeSchedules.OrderBy(se => se.Date)
                                    .ThenBy(se => se.Start)
                                    .ToList();
        }


        // GET api/schedules
        public List<Schedule> GetEmployeeSchedules(int? employeeId, bool? selected)
        {
            HttpStatusCode httpStatusCode;

            if (employeeId != null)
            {
                var employeeSchedulesUri = new Uri(string.Format(_employeeSchedules, int.Parse(employeeId.ToString()), _clientToken));

                var employeeSchedules = Invoke.Get<List<Schedule>>(employeeSchedulesUri, out httpStatusCode);

                var employeeSchedulesDto = employeeSchedules.OrderBy(s => s.Date)
                                                            .Select(s => new Schedule
                                                            {
                                                                Id = s.Id,
                                                                Date = s.Date,
                                                                DayName = s.Date.ToString("dddd"),
                                                                Start = s.Start,
                                                                StartTime = s.Start.ToString("h:mm tt"),
                                                                End = s.End,
                                                                EndTime = s.End.ToString("h:mm tt"),
                                                                ShiftTypeId = s.ShiftTypeId,
                                                                ShiftTypeName = s.ShiftType.Name,
                                                                ShiftTypeDescription = s.ShiftTypeDescription,
                                                                ShiftType = s.ShiftType,
                                                                PriorityId = s.PriorityId,
                                                                SeasonName = s.SeasonName,
                                                                SeasonDescription = s.SeasonDescription,
                                                                SeasonId = s.SeasonId
                                                            });

                return employeeSchedulesDto.ToList();
            }
            else
            {
                if (selected != null)
                {
                    var allSchedulesUri = new Uri(string.Format(_allSchedules, _clientToken));

                    var allSchedules = Invoke.Get<List<Schedule>>(allSchedulesUri, out httpStatusCode);

                    var selectedSchedules = allSchedules.Where(s => s.EmployeeId != null)
                                                        .OrderBy(s => s.Date).ToList()
                                                        .Select(s => new Schedule
                                                        {
                                                            Id = s.Id,
                                                            Date = s.Date,
                                                            DayName = s.Date.ToString("dddd"),
                                                            Start = s.Start,
                                                            StartTime = s.Start.ToString("h:mm tt"),
                                                            End = s.End,
                                                            EndTime = s.End.ToString("h:mm tt"),
                                                            ShiftTypeId = s.ShiftTypeId,
                                                            ShiftTypeName = s.ShiftType.Name,
                                                            ShiftTypeDescription = s.ShiftTypeDescription,
                                                            PriorityId = s.PriorityId,
                                                            SeasonName = s.SeasonName,
                                                            SeasonDescription = s.SeasonDescription,
                                                            SeasonId = s.SeasonId
                                                        });

                    return selectedSchedules.ToList();
                }
                else
                {
                    var availableSchedulesUri = new Uri(string.Format(_availableSchedules, _clientToken));

                    var availableSchedules = Invoke.Get<List<Schedule>>(availableSchedulesUri, out httpStatusCode);

                    return availableSchedules.OrderBy(s => s.Date).ToList();
                }
            }
        }

        // GET api/scheduleCalendar
        public List<Event> GetScheduleCalendar(bool calendar)
        {
            HttpStatusCode httpStatusCode;

            var allSchedulesUri = new Uri(string.Format(_allSchedules, _clientToken));

            var allSchedules = Invoke.Get<List<Schedule>>(allSchedulesUri, out httpStatusCode);

            // var employeeSchedules = allSchedules.Where(s => s.EmployeeId != null);

            var selectedSchedules = allSchedules.GroupBy(l => new { l.Date, l.Start, l.ShiftType.Id })
                                   .Select(l => new Event
                                   {
                                       id = l.Max(s => s.Id),
                                       title = l.FirstOrDefault().ShiftType.Description + "(" + 
                                               l.FirstOrDefault().Start.ToString("h:mm") + " - " + 
                                               l.FirstOrDefault().End.ToString("h:mm") + ") - " +
                                               l.Count(s => s.EmployeeId != null).ToString() + " of " + 
                                               l.Count().ToString(),
                                       start = l.FirstOrDefault().Start,
                                       end = l.FirstOrDefault().End,
                                       color = l.Count(s => s.EmployeeId != null) == l.Count() ? "green" :
                                                    l.Count(s => s.EmployeeId != null) == 0 ? "red" : "yellow"
                                   });
                                       
            return selectedSchedules.ToList();

        }

        // GET api/schedules?year=2013&month=12&day=25
        public List<EmployeeSchedule> GetSchedulesBy(int? year, int? month, int? day)
        {
            var scheduleYear = year == null ? DateTime.Now.Year : int.Parse(year.ToString());

            var scheduleMonth = month == null ? DateTime.Now.Month : int.Parse(month.ToString());

            var scheduleDay = day == null ? DateTime.Now.Day : int.Parse(day.ToString());

            HttpStatusCode httpStatusCode;

            var schedulesByDateUri = new Uri(string.Format(_schedulesByDateUrl, scheduleYear, scheduleMonth, scheduleDay, _clientToken));

            var schedulesByDate = Invoke.Get<List<Schedule>>(schedulesByDateUri, out httpStatusCode);

            var schedules = schedulesByDate.Where(s => s.EmployeeId != null)
                                           .OrderBy(s => s.ShiftType.Id);

            var employeesUri = new Uri(string.Format(_employeesUrl, _clientToken));

            var employees = Invoke.Get<List<Employee>>(employeesUri, out httpStatusCode);

            var employeeSchedules = schedules.Join(employees, s => s.EmployeeId, e => e.Id, (schedule, employee) => new { schedule, employee })
                                             .Where(se => se.schedule.EmployeeId == se.employee.Id)
                                             .Select(s => new EmployeeSchedule
                                             {
                                                 Date = s.schedule.Date,
                                                 Start = s.schedule.Start,
                                                 StartTime = s.schedule.Start.ToString("h:mm tt"),
                                                 End = s.schedule.End,
                                                 EndTime = s.schedule.End.ToString("h:mm tt"),
                                                 ShiftType = s.schedule.ShiftType,
                                                 EmployeeId = s.employee.Id,
                                                 FirstName = s.employee.Person.FirstName,
                                                 LastName = s.employee.Person.LastName
                                             });

            return employeeSchedules.ToList();
        }

        // GET api/schedules?year=2013&month=12&day=25
        public List<Shift> GetSchedulesBy(int? year, int? month, int? day, bool printable)
        {
            var scheduleYear = year == null ? DateTime.Now.Year : int.Parse(year.ToString());

            var scheduleMonth = month == null ? DateTime.Now.Month : int.Parse(month.ToString());

            var scheduleDay = day == null ? DateTime.Now.Day : int.Parse(day.ToString());

            HttpStatusCode httpStatusCode;

            var schedulesByDateUri = new Uri(string.Format(_schedulesByDateUrl, scheduleYear, scheduleMonth, scheduleDay, _clientToken));

            var schedulesByDate = Invoke.Get<List<Schedule>>(schedulesByDateUri, out httpStatusCode);

            var schedules = schedulesByDate.Where(s => s.EmployeeId != null)
                                           .OrderBy(s => s.ShiftType.Id);

            var employeesUri = new Uri(string.Format(_employeesUrl, _clientToken));

            var employees = Invoke.Get<List<Employee>>(employeesUri, out httpStatusCode);

            var employeeSchedules = schedules.Join(employees, s => s.EmployeeId, e => e.Id, (schedule, employee) => new { schedule, employee })
                                             .Where(se => se.schedule.EmployeeId == se.employee.Id)
                                             .OrderBy(s => s.employee.RosterId)
                                             .Select(s => new EmployeeSchedule
                                             {
                                                 RosterId = s.employee.RosterId,
                                                 Date = s.schedule.Date,
                                                 Start = s.schedule.Start,
                                                 StartTime = s.schedule.Start.ToString("h:mm tt"),
                                                 End = s.schedule.End,
                                                 EndTime = s.schedule.End.ToString("h:mm tt"),
                                                 ShiftType = s.schedule.ShiftType,
                                                 EmployeeId = s.employee.Id,
                                                 FirstName = s.employee.Person.FirstName,
                                                 LastName = s.employee.Person.LastName
                                             }).ToList();

            var shifts = employeeSchedules.GroupBy(s => s.Start)
                                          .Select(s => new Shift
                                                {
                                                    Name = s.First().StartTime + " - " + s.First().EndTime,
                                                    Date = s.First().Date,
                                                    End = s.First().End,
                                                    EndTime = s.First().EndTime,
                                                    Start = s.First().Start,
                                                    StartTime = s.First().StartTime,
                                                    Id = s.First().Id
                                                }).ToList();

            var shiftGroups = employeeSchedules.GroupBy(s => s.ShiftType.Description)
                                       .Select(s => new ShiftGroup
                                       {
                                           Name = s.First().ShiftType.Name,
                                           Description = s.First().ShiftType.Description,
                                           Id = s.First().ShiftType.Id
                                       }).ToList();

            var viewShifts = new List<Shift>();

            foreach (var shift in shifts.Where(s => employeeSchedules.Select(se => se.StartTime).Contains(s.StartTime)))
            {
                var xShift = new Shift()
                {
                    Date = shift.Date,
                    End = shift.End,
                    EndTime = shift.EndTime,
                    Start = shift.Start,
                    StartTime = shift.StartTime,
                    Id = shift.Id,
                    Name = shift.Name
                };

                var xShiftGroups = new List<ShiftGroup>();

                foreach (var shiftGroup in shiftGroups.Where(s => employeeSchedules.Select(sg => sg.ShiftType.Id).Contains(s.Id)))
                {
                    var xShiftGroup = new ShiftGroup()
                    {
                        Name = shiftGroup.Name,
                        Description = shiftGroup.Description,
                        Id = shiftGroup.Id
                    };

                    var xSchedules = new List<EmployeeSchedule>();

                    xShiftGroup.Schedules = employeeSchedules.Where(s => s.ShiftType.Id == shiftGroup.Id && s.Start == shift.Start).ToList();

                    xShiftGroups.Add(xShiftGroup);
                }

                xShift.ShiftGroups = xShiftGroups;

                viewShifts.Add(xShift);
            }

            return viewShifts;
            //return employeeSchedules.ToList();
        }

        // GET api/schedules
        public List<Schedule> GetAllSchedules(bool grouped)
        {
            HttpStatusCode httpStatusCode;

            var allSchedulesUri = new Uri(string.Format(_allSchedules, _clientToken));

            var allSchedules = Invoke.Get<List<Schedule>>(allSchedulesUri, out httpStatusCode);


            if (grouped)
            {
                var groupedSchedules = from s in allSchedules
                                       group s by new { s.Date, s.Start, s.ShiftTypeId } into grp
                                       select new Schedule()
                                       {
                                           Id = grp.Max(t => t.Id),
                                           Date = grp.FirstOrDefault().Date,
                                           MonthName = grp.FirstOrDefault().Date.ToString("MMMM"),
                                           DayName = grp.FirstOrDefault().Date.DayOfWeek.ToString(),
                                           Start = grp.FirstOrDefault().Start,
                                           StartTime = grp.FirstOrDefault().Start.ToString("h:mm tt"),
                                           End = grp.FirstOrDefault().End,
                                           EndTime = grp.FirstOrDefault().End.ToString("h:mm tt"),
                                           ShiftTypeId = grp.FirstOrDefault().ShiftTypeId,
                                           ShiftTypeName = grp.FirstOrDefault().ShiftType.Name,
                                           ShiftTypeDescription = grp.FirstOrDefault().ShiftTypeDescription,
                                           PriorityId = grp.FirstOrDefault().PriorityId,
                                           SeasonName = grp.FirstOrDefault().SeasonName,
                                           SeasonDescription = grp.FirstOrDefault().SeasonDescription,
                                           SeasonId = grp.FirstOrDefault().SeasonId,
                                           Count = grp.Count()
                                       };

                return groupedSchedules.OrderBy(s => s.Date)
                                       .ThenBy(s => s.Start)
                                       .ThenBy(s => s.ShiftTypeId)
                                       .ToList();
            }
            else
            {
                return allSchedules
                                   .OrderBy(s => s.Date)
                                   .ThenBy(s => s.Start)
                                   .ThenBy(s => s.ShiftTypeId)
                                   .Select(s => new Schedule() {
                                       Id = s.Id,
                                       Date = s.Date,
                                       Start = s.Start,
                                       StartTime = s.Start.ToString("h:mm tt"),
                                       End = s.End,
                                       EndTime = s.End.ToString("h:mm tt"),
                                       ShiftType = s.ShiftType,
                                       PriorityId = s.PriorityId,
                                       Season = s.Season
                                   })
                                   .ToList();
            }
        }

        // GET api/schedules?
        public List<Schedule> GetAvailableSchedules(int shiftType, int employeeId, int? month)
        {
            HttpStatusCode httpStatusCode;

            var availableSchedulesUri = new Uri(string.Format(_availableSchedules, _clientToken));

            var availableSchedules = Invoke.Get<List<Schedule>>(availableSchedulesUri, out httpStatusCode);

            var employeeSchedulesUri = new Uri(string.Format(_employeeSchedules, int.Parse(employeeId.ToString()), _clientToken));

            var employeeSchedules = Invoke.Get<List<Schedule>>(employeeSchedulesUri, out httpStatusCode);

            var employeeSelectedDates = employeeSchedules.Select(e => e.Date);

            var availableSchedulesByType = availableSchedules.Where(s => s.ShiftTypeId == shiftType && !employeeSelectedDates.Contains(s.Date));

            var groupedAvailableSchedulesByType = from s in availableSchedulesByType
                                                  where s.Date > DateTime.Now
                                                  group s by new { s.Date, s.Start } into grp
                                                  select new Schedule()
                                                  {
                                                      Id = grp.Max(t => t.Id),
                                                      Date = grp.FirstOrDefault().Date,
                                                      Start = grp.FirstOrDefault().Start,
                                                      StartTime = grp.FirstOrDefault().Start.ToString("h:mm tt"),
                                                      End = grp.FirstOrDefault().End,
                                                      EndTime = grp.FirstOrDefault().End.ToString("h:mm tt"),
                                                      ShiftTypeId = grp.FirstOrDefault().ShiftTypeId,
                                                      PriorityId = grp.FirstOrDefault().PriorityId,
                                                      SeasonId = grp.FirstOrDefault().SeasonId,
                                                      Count = grp.Count()
                                                  };

            return month != null ? 
                groupedAvailableSchedulesByType.Where(s => s.Date.Month == month).OrderBy(s => s.Date).ToList() : 
                groupedAvailableSchedulesByType.OrderBy(s => s.Date).ToList();
        }

        // DELETE api/schedules?id=
        public Schedule Delete([FromUri]int id)
        {
            HttpStatusCode httpStatusCode;

            var deleteScheduleUri = new Uri(string.Format(_deleteScheduleUrl, id, _clientToken));

            var deletedSchedule = Invoke.Delete<Schedule>(deleteScheduleUri, out httpStatusCode);

            return deletedSchedule;
        }

        // POST api/schedules
        public Schedule Post([FromBody]Schedule schedule)
        {
            HttpStatusCode httpStatusCode;

            var scheduleTimeUri = new Uri(string.Format(_scheduleTimeUrl, schedule.ScheduleTimeId, _clientToken));

            var scheduleTime = Invoke.Get<ScheduleTime>(scheduleTimeUri, out httpStatusCode);

            var scheduleStart = scheduleTime.Start;

            var scheduleEnd = scheduleTime.End;

            var scheduleCount = schedule.Count;

            var newSchedule = new Schedule()
            {
                ClientId = 0,
                Date = new DateTime(schedule.Date.Year, schedule.Date.Month, schedule.Date.Day),
                Start = new DateTime(schedule.Date.Year, schedule.Date.Month, schedule.Date.Day, scheduleStart.Hour, scheduleStart.Minute, scheduleStart.Second),
                End = new DateTime(schedule.Date.Year, schedule.Date.Month, schedule.Date.Day, scheduleEnd.Hour, scheduleEnd.Minute, scheduleEnd.Second),
                ShiftType = new ShiftType() { Id = schedule.ShiftTypeId, Name = "Test Name", Description = "Test Description" },
                Season = new Season() { Id = 4, Name = "Test Name", Description = "Test Description", Start = DateTime.Now, End = DateTime.Now.AddDays(100) },
                SeasonId = 4,
                PriorityId = 4,
                Priority = new Priority() { Id = 4, Name = "Test Name", Description = "Test Description" },
                CanAdd = true,
                CanRemove = true,
                CanUpdate = true,
                Assigned = false
            };

            var postScheduleUri = new Uri(string.Format(_postSchedule, _clientToken));

            var postedSchedule = new Schedule(); 

            Parallel.For(0, scheduleCount, i =>
                {
                    postedSchedule = Invoke.Post<Schedule>(postScheduleUri, newSchedule, out httpStatusCode);
                });

            postedSchedule.Count = scheduleCount;

            return postedSchedule;
        }

        // PUT api/schedules?id={scheduleId}
        public Schedule Put([FromUri]int scheduleId, [FromBody]Schedule schedule)
        {
            HttpStatusCode httpStatusCode;

            var updateScheduleWithEmployeeIdUri = new Uri(string.Format(_updateScheduleWithEmployeeId, scheduleId, _clientToken));

            var updatedSchedule = Invoke.Put<Schedule>(updateScheduleWithEmployeeIdUri, schedule, out httpStatusCode);

            return updatedSchedule;
        }

    }
}

