﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using SkiSchool.Web.App_Start;
using SkiSchool.Web.Helpers;
using SkiSchool.Web.Models;
using WebMatrix.WebData;

namespace SkiSchool.Web.Controllers.Api
{
    public class EmployeesController : ApiController
    {
        private readonly string _clientToken = Config.ClientToken;

        private string _employeeWithLoginIdUrl = ApiRoutes.EmployeeWithLoginIdUrl;

        private string _employeeWithIdUrl = ApiRoutes.EmployeeWithIdUrl;

        private string _employeesUrl = ApiRoutes.Employees;

        private string _employeeUpdateUrl = ApiRoutes.UpdateEmployeeUrl;

        private string _employeeAddUrl = ApiRoutes.AddEmployee;

        private string _allSchedules = ApiRoutes.AllSchedules;

        private string _personAddUrl = ApiRoutes.AddPerson;

        private readonly UsersContext _usersContext;

        public EmployeesController()
        {
            _usersContext = new UsersContext();
        }

        // GET api/employees
        public List<Employee> GetAll()
        {
            HttpStatusCode httpStatusCode;

            var employeesUri = new Uri(string.Format(_employeesUrl, _clientToken));

            var employees = Invoke.Get<List<Employee>>(employeesUri, out httpStatusCode);

            var allSchedulesUri = new Uri(string.Format(_allSchedules, _clientToken));

            var allSchedules = Invoke.Get<List<Schedule>>(allSchedulesUri, out httpStatusCode);

            var employeeSchedules = allSchedules.Where(s => s.EmployeeId != null)
                                        .GroupBy(s => new { s.EmployeeId })
                                        .Select(s => new Schedule
                                        {
                                            Count = s.Count(),
                                            EmployeeId = s.Key.EmployeeId
                                        }).ToList();

            var employeesWithScheduleCount = employees.Join(employeeSchedules, 
                                                            e => e.Id, 
                                                            s => s.EmployeeId, 
                                                            (e, s) => new Employee
            {
                ClientToken = e.ClientToken,
                Current = e.Current,
                EmployeeTitle = e.EmployeeTitle,
                EmployeeType = e.EmployeeType,
                Id = e.Id,
                IsLocal = e.IsLocal,
                LoginId = e.LoginId,
                Person = e.Person,
                RosterId = e.RosterId,
                ScheduleCount = s.Count,
                ScheduleCountOfTotal = string.Format("{0} of {1}", s.Count, e.IsLocal == true ? 32 : 20)
            }).ToList();

            var employeeIdsWithSchedules = employeesWithScheduleCount.Select(e => e.Id).ToList();

            var employeesWithoutSchedules = employees.Where(e => !employeeIdsWithSchedules.Contains(e.Id)).ToList();

            var allEmployees = employeesWithScheduleCount.Union(employeesWithoutSchedules).ToList();

            return allEmployees.OrderBy(e => e.Person.LastName).ToList();
        }

        // PUT api/employees?{id}
        [HttpPut]
        public Employee Put([FromUri]int id, [FromBody]Employee employee)
        {
            HttpStatusCode httpStatusCode;

            var employeeUpdateUrl = string.Format(_employeeUpdateUrl, id, _clientToken);

            var employeeUpdateUri = new Uri(employeeUpdateUrl);

            var updatedEmployee = Invoke.Put<Employee>(employeeUpdateUri, employee, out httpStatusCode);

            return updatedEmployee;
        }

        private UserEmployeeInfo AddUser(UserEmployeeInfo userEmployeeInfo)
        {
            var user = _usersContext.UserProfiles.FirstOrDefault(u => u.UserName == userEmployeeInfo.Username);

            if (user == null)
            {
                WebSecurity.CreateUserAndAccount(userEmployeeInfo.Username, userEmployeeInfo.Password, new { ClientToken = Guid.Parse(_clientToken) });

                user = _usersContext.UserProfiles.FirstOrDefault(u => u.UserName == userEmployeeInfo.Username);

                Roles.AddUserToRole(user.UserName, userEmployeeInfo.RoleName);

                return new UserEmployeeInfo
                {
                    Username = user.UserName,
                    UserId = user.UserId
                };
            }

            else
                return null;
        }

        private Person AddPerson(Person person)
        {
            HttpStatusCode httpStatusCode;

            var personAddUrl = string.Format(_personAddUrl, _clientToken);

            var personAddUri = new Uri(personAddUrl);

            var addedPerson = Invoke.Post<Person>(personAddUri, person, out httpStatusCode);

            return addedPerson;
        }

        private Employee AddEmployee(Employee employee)
        {
            HttpStatusCode httpStatusCode;

            var employeeAddUrl = string.Format(_employeeAddUrl, _clientToken);

            var employeeAddUri = new Uri(employeeAddUrl);

            var addedEmployee = Invoke.Post<Employee>(employeeAddUri, employee, out httpStatusCode);

            return addedEmployee;
        }

        private bool ValidateNewEmployee(Employee employee)
        {
            if (employee == null)
                return false;

            if (employee.RosterId.Length == 0)
                return false;

            if (employee.EmployeeType.Id == 0)
                return false;

            if (employee.EmployeeTitle.Id == 0)
                return false;

            if (employee.UserEmployeeInfo == null)
                return false;

            if (employee.UserEmployeeInfo.Username.Length == 0)
                return false;

            if (employee.UserEmployeeInfo.Password.Length == 0)
                return false;

            if (employee.UserEmployeeInfo.RoleName.Length == 0)
                return false;

            if (employee.Person.LastName.Length == 0)
                return false;

            if (employee.Person.FirstName.Length == 0)
                return false;

            if (employee.Person.Gender.Id == 0)
                return false;

            return true;
        }

        // POST api/employees
        [HttpPost]
        public Employee Post([FromBody]Employee employee)
        {
            if (!ValidateNewEmployee(employee))
                return null;


            // Add User
            var userEmployeeInfoToAdd = new UserEmployeeInfo
            {
                Username = employee.UserEmployeeInfo.Username,
                Password = employee.UserEmployeeInfo.Password,
                RoleName = employee.UserEmployeeInfo.RoleName
            };

            var addedUserEmployeeInfo = AddUser(userEmployeeInfoToAdd);

            // Add Person
            var personToAdd = new Person
            {
                FirstName = employee.Person.FirstName,
                LastName = employee.Person.LastName,
                MiddleName = employee.Person.MiddleName,
                DateOfBirth = employee.Person.DateOfBirth,
                GenderId = employee.Person.Gender.Id
            };

            var addedPerson = AddPerson(personToAdd);

            // Add Employee
            var employeeToAdd = new Employee
            {
                Current = employee.Current,
                EmployeeTypeId = employee.EmployeeType.Id,
                TitleId = employee.EmployeeTitle.Id,
                IsLocal = employee.IsLocal,
                LoginId = addedUserEmployeeInfo.UserId,
                PersonId = addedPerson.Id,
                RosterId = employee.RosterId
            };

            var addedEmployee = AddEmployee(employeeToAdd);

            return addedEmployee;
        }

        // GET api/employees/5
        public Employee Get(int? loginId, int? id)
        {
            HttpStatusCode httpStatusCode;

            if (id == null)
            {
                var employeeWithLoginIdUri = new Uri(string.Format(_employeeWithLoginIdUrl, loginId, _clientToken));

                var employee = Invoke.Get<Employee>(employeeWithLoginIdUri, out httpStatusCode);

                return employee;
            }

            if (loginId == null)
            {
                var employeeWithIdUri = new Uri(string.Format(_employeeWithIdUrl, id, _clientToken));

                var employee = Invoke.Get<Employee>(employeeWithIdUri, out httpStatusCode);

                return employee;
            }

            return new Employee();
        }
    }
}

